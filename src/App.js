import React, { useContext, useState } from 'react'

// Components
import Environment from './components/environment/'
import MainHeader from './components/main-header'
import Infos from './components/infos'
import Subtitles from './components/subtitles'
import Controls from './components/controls'
import ChartOverall from './components/chartOverall'

import EnvironmentContextProvider from './services/contexts/environment'

import Style from './style'

function App() {

  const environmentParamsDefault = {
    peopleInit: {
      total: 60,
      stoppedPercent: .1,
      maskedPercent: .15,
      infectedPercent: .4,
    },
    params: {
      infectionTax: .5,
      maskProtectionTax: .9,
      deathTax: .1,
      invencibleTime: 1000,
      contactRadius: 32, 

      virusTime: 15000,
      virusTimeRange: .2,

      conscienceTax: 0.2,
      stoppedTaxRate: .2,
      useMaskTaxRate: .5,
    }
  }

  const [startSimulation, setStartSimulation] = useState(false)
  const [customEnvironmentParams, setCustomEnvironmentParams] = useState(environmentParamsDefault)

  const initSimulation = (params) => {
    setStartSimulation(true)
    params && setCustomEnvironmentParams(params)
  }

  const restartSimulation = () => {
    setStartSimulation(false)
  }

  return (
    <Style className="App">
      <main>
        <MainHeader />
        <EnvironmentContextProvider>
        {
          startSimulation && (
            <div className="main-content container">
            <div className="left-side">
              <Environment
                containerSize={{
                  width: 360,
                  height: 360,
                }}
                {...customEnvironmentParams}
              />
              <Subtitles />
            </div>
            <div className="right-side mobile_up-side">
              <Infos />
              <ChartOverall />
            </div>
            </div>
          )
        }
        <Controls 
          startSimulation={initSimulation} 
          restartSimulation={restartSimulation} 
          defaultParams={environmentParamsDefault} 
        />
        </EnvironmentContextProvider>
      </main>
    </Style>
  );
}

export default App;
