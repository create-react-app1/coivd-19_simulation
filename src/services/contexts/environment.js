import React, { useState, createContext } from 'react'

export const EnvironmentContext = createContext()

export default ({children}) => {

  const [size, setSize] = useState({width: 'auto', height: 'auto'})

  const [totalPeople, setTotalPeople] = useState(0)
  const [qttDeathsPeople, setQttDeathsPeople] = useState(0)
  const [qttAlivesPeople, setQttAlivesPeople] = useState(0)
  const [qttInfectedsPeople, setQttInfectedsPeople] = useState(0)
  const [qttMaskedsPeople, setQttMaskedsPeople] = useState(0)
  const [qttStoppedsPeople, setQttStoppedsPeople] = useState(0)

  const [steps, setSteps] = useState(1)

  const changeSize = (newSize) => {
    setSize({...size, ...newSize})
  }

  return (
    <EnvironmentContext.Provider value={{
      container: {
        size, changeSize,
      },

      people: {
        total: totalPeople, setTotal: setTotalPeople,
        qttDeaths: qttDeathsPeople, setQttDeaths: setQttDeathsPeople,
        qttAlives: qttAlivesPeople, setQttAlives: setQttAlivesPeople,
        qttInfecteds: qttInfectedsPeople, setQttInfecteds: setQttInfectedsPeople,
        qttMaskeds: qttMaskedsPeople, setQttMaskeds: setQttMaskedsPeople,
        qttStoppeds: qttStoppedsPeople, setQttStoppeds: setQttStoppedsPeople
      },

      steps,
      setSteps,

    }}>
      {children}
    </EnvironmentContext.Provider>
  )

}