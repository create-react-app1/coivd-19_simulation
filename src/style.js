import styled from 'styled-components'

export default styled.div`

  main {
    font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  }

  .left-side { grid-area: left-side; display: grid; gap: 16px; }
  .right-side { grid-area: right-side; display: grid; gap: 16px; }

  .main-content {
    display: grid;
    grid-template-columns: 360px 1fr;
    grid-template-rows: auto;

    gap: 16px;
    grid-template-areas: 
      "left-side right-side" 
  }

  .grid-wrapper {
    background-color: #f1f1f1;
    width: 100%;
    height: auto;
    border-radius: 5px;
    box-shadow: 0px 0px 1px 1px #0000003d;
  }

  .grid-block {
    padding: 10px;
  }

  .container {
    max-width: 1200px;
    margin: 0 auto;
  }

  .grid-title {
    font-weight: bold;
    margin: 10px 10px 20px 10px;
  }

  .grid-content {
    padding: 10px;
    background-color: #ffffff69;
    border-radius: 5px;
    font-size: 12px;
  }

  .icon-button > span {
    background-color: #3f51b5 !important;
    color: #e2e2e2 !important;
  }

  .icon-button {
    display: none;
  }

  @media (max-width: 850px) {

    .main-content {
      display: grid;
      grid-template-columns: 1fr;
      grid-template-rows: auto;
  
      gap: 16px;
      grid-template-areas:
        "mobile_up-side"
        "left-side"; 
    }

    .mobile_up-side { grid-area: mobile_up-side; display: grid; gap: 16px; }

    .mobile_up-side .chart, .control-start-sim {
      position: fixed;
      bottom: -110%;

      transition: .5s;
    }

    .mobile_up-side .chart.show, .control-start-sim.show {
      bottom: 0px;
      z-index: 100000;
    }

    .icon-button {
      display: block;
      z-index: 1000;
    }

    .icon-button.pulse-button.pulse {
      border-radius: 50%;
      box-shadow: 0 0 0 0 #3f51b5aa;

      animation: pulse 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
    }

    @keyframes pulse {
      to {box-shadow: 0 0 0 35px rgba(232, 76, 61, 0);}
    }

  }

`