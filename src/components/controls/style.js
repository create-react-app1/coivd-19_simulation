import styled from 'styled-components'

export default styled.div`

  margin: -8px;
  width: auto !important;

  .grid-block {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;

    margin-top: 25px !important;
  }

  .controls-input {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-evenly;

    padding: 20px 10px;
  }

  .summary {
    padding: 20px 10px;
    background: #55695533;
    border-radius: 5px;
    p {
      font-size: 14px;
      margin-left: 20px;
      margin-right: 20px;
      line-height: 25px;
      text-align: justify;
    }
    .underline {
      text-decoration: underline;
    }
  }

  .controls-input > * {
    margin: 10px;
  }

  .controls-input p {
    margin: 1px;
  }

  hr {
    width: 100%;
    border-color: #0000001f;
  }

  .grid-title {
    width: 100%;
  }

  button {
    margin: 20px;
  }

  @media (max-width: 1150px){
    .input-wrapper {
      width: 20%;
    }
  }

  @media (max-width: 850px) {
    .input-wrapper {
      width: 250px;
    }

    .icon-button.control-toggle-button:not(.show) {
      position: fixed;
      bottom: 0px;
      left: 0px;

      transition: .5s;
    }

    .icon-button.control-toggle-button.show {
      position: absolute;
      margin: 5px;
    }

    .btn-restart {
      position: fixed;
      bottom: 5px;
      margin: 0px !important;
    }

  }

`