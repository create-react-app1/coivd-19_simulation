import React, { useState } from 'react'

import Button from '../inputs/button' 
import Slider from '../inputs/slider' 
import { IconButton } from '@material-ui/core';

import {
  GpsFixed, // stop
  Snooze, // stop
  Block,
  PeopleOutline,
  Security, //mask tax
  VerifiedUser, // mask
  PersonAdd,
  RecordVoiceOver, // contact
  ScatterPlot, // virus,
  Timer, 
  Timelapse,
  TrackChanges, // radius
  TrendingDown, 
  TrendingUp,

  Settings,
  PlayArrow,
  Replay,
} from '@material-ui/icons/';

import Style from './style'

// Reference
// https://material-ui.com/pt/getting-started/installation/
// https://material-ui.com/pt/components/buttons/

export default ({startSimulation, restartSimulation, defaultParams}) => {

  const [controlParams, setControlParams] = useState(defaultParams)
  const [startSim, setStartSim] = useState(false)
  const [showContent, setShowContent] = useState(true)
  const [btnPulse, setBtnPulse] = useState(true)

  // peopleInit={{
  //   total: 60,
  //   stoppedPercent: .1,
  //   maskedPercent: .15,
  //   infectedPercent: .4,
  // }}

  // params={{
  //   infectionTax: .5,
  //   maskProtectionTax: .9,
  //   deathTax: .15,
  //   invencibleTime: 1000,
  //   contactRadius: 32, 

  //   virusTime: 15000,
  //   virusTimeRange: .2,

  //   conscienceTax: 0.2,
  //   stoppedTaxRate: .2,
  //   useMaskTaxRate: .5,
  // }}

  // BONUS
  // PreSet Mode: Random, Worst, Bad, Normal, Good

  // Normal
  // peopleInit={{
  //   total: 40,
  //   stoppedPercent: .35,
  //   maskedPercent: .7,
  //   infectedPercent: .2,
  // }}
  // Bad
  // peopleInit={{
  //   total: 60,
  //   stoppedPercent: .1,
  //   maskedPercent: .15,
  //   infectedPercent: .4,
  // }}
  // Worst
  // peopleInit={{
  //   total: 100,
  //   stoppedPercent: .05,
  //   maskedPercent: .1,
  //   infectedPercent: .5,
  // }}
  // Good
  // peopleInit={{
  //   total: 30,
  //   stoppedPercent: .45,
  //   maskedPercent: .8,
  //   infectedPercent: .1,
  // }}

  const setStartSimulation = () => {
    window.scrollTo(0,0)
    window.innerWidth < 850 && setShowContent(false)
    setStartSim(true)
    startSimulation(controlParams)
  }

  const setRestartSimulation = () => {
    window.location.reload()
    // setStartSim(false)
    // restartSimulation()
  }

  const onChange = (value, group, field) => {
    const groupLastState = controlParams[group]
    const newControlParams = {
      ...controlParams,
      [group]: {
        ...groupLastState,
        [field]: value
      }
    }
    setControlParams(newControlParams)

    if(startSim) startSimulation(newControlParams)
  }

  const { peopleInit, params } = defaultParams

  const slidersConfig = [
    // Principais
    {
      sliderOptions: {min: 10, max: 200, step: 10, marks: [], },
      label: 'Total de Pessoas', description: 'Pessoas que começarão no ambiente.',
      initValue: peopleInit.total,
      Icon: PersonAdd,
      onChange: (value) => onChange(value, 'peopleInit', 'total'),
    },
    {
      label: 'Confinadas %', description: 'Porcentagem de pessoas que começarão confinadas, ou seja, sem se mover.',
      initValue: peopleInit.stoppedPercent * 100,
      Icon: GpsFixed,
      onChange: (value) => onChange(value/100, 'peopleInit', 'stoppedPercent')
    },
    {
      label: 'Com Máscaras %', description: 'Porcentagem de pessoas que começarão usando máscara.',
      initValue: peopleInit.maskedPercent * 100,
      Icon: VerifiedUser,
      onChange: (value) => onChange(value/100, 'peopleInit', 'maskedPercent')
    },
    {
      label: 'Infectadas %', description: 'Porcentagem de pessoas que começarão infectadas com o vírus.',
      initValue: peopleInit.infectedPercent * 100,
      Icon: PeopleOutline,
      onChange: (value) => onChange(value/100, 'peopleInit', 'infectedPercent')
    },

    // Secundárias (TODO: poder mudar esses valores depois de iniciado)
    {
      sliderOptions: {min: 20, max: 100, step: 10, marks: [], },
      label: 'Taxa de Infecção %', description: 'Chance de uma pessoa infectar outra ao haver contato.',
      initValue: params.infectionTax * 100,
      Icon: ScatterPlot,
      onChange: (value) => onChange(value/100, 'params', 'infectionTax')
    },
    {
      sliderOptions: {min: 50, max: 99, step: 10, marks: [], },
      label: 'Taxa de Proteção da Máscara %', description: 'Chance da máscara proteger de uma infecção, quando houver contato.',
      initValue: params.maskProtectionTax * 100,
      Icon: Security,
      onChange: (value) => onChange(value/100, 'params', 'maskProtectionTax')
    },
    {
      sliderOptions: {min: 5, max: 80, step: 5, marks: [], },
      label: 'Taxa de Morte %', description: 'Chance de morrer ao contrair o vírus.',
      initValue: params.deathTax * 100,
      Icon: TrendingDown,
      onChange: (value) => onChange(value/100, 'params', 'deathTax')
    },
    {
      sliderOptions: {min: 0, max: 100, step: 10, marks: [], },
      label: 'Taxa de Consciencia %', description: 'Chance das pessoas tomarem boas ações (ficarem isoladas e/ou usar máscara).',
      initValue: params.conscienceTax * 100,
      Icon: TrendingUp,
      onChange: (value) => onChange(value/100, 'params', 'conscienceTax')
    },

    // Menos Importantes
  ]

  const initialInputs = slidersConfig.slice(0,4)
  const onGameInputs = slidersConfig.slice(4,8)

  return (
    <Style className={`control grid-wrapper ${startSim && 'control-start-sim'} ${showContent && 'show'}`}>

      {
        startSim && (
          <div className={`
          icon-button control-toggle-button 
          ${showContent && 'show'}
          pulse-button ${btnPulse && 'pulse'}`}>
            <IconButton
              variant="contained"
              onClick={() => {setShowContent(!showContent); setBtnPulse(false)}} 
              color="primary" aria-label="upload picture" component="span"
            >
              <Settings />
            </IconButton>
          </div>
        )
      }
      
      <div className="grid-block container">
        {
          !startSim && (
            <>
            <div className="summary">
              <div className="grid-title">Sobre o Simulador</div>
              <p>O simulador do vírus COIVD-19 foi criado para conscientizar as pessoas da importância de <strong>usar máscara</strong> e <strong>ficar em confinamento</strong>, assim como é reportado em diversas mídias.</p>
              <p>Os parâmetros abaixo serão usados para dar vida a simulação, nos <strong className="underline">Valores de Criação do Ambiente</strong> será possível configurar a quantidade <strong>Total de Pessoas</strong> nesse ambiente (lembrando que quanto maior o número de pessoas, maior a densidade demográfica), a porcentagem de Pessoas <strong>Confinadas</strong> (fazendo alusão ao movimento <strong>Fique em Casa</strong>), a porcentagem de Pessoas <strong>Com Máscara</strong> e a porcentagem de Pessoas <strong>Infectadas</strong>.</p>
              <p>Os <strong className="underline">Valores Alteráveis Durante a Simulação</strong> estarão sempre disponíveis para serem modificados. Como por exemplo a <strong>Taxa de Consciencia</strong>, fazem com que a população comece a se proteger e tomar mais cuidado, ficando em confinamento ou usando máscaras. Modifique os parâmetros e veja seu reflexo em tempo real no ambiente.</p>
            </div>
            <hr />
            <div className="controls-input">
              <div className="grid-title">Valores de Criação do Ambiente</div>
              { initialInputs.map((s, i) => <Slider key={`slider-${i}`} {...s} />) }
            </div>
            <hr />
            </>
          )
        }
        <div className="controls-input">
          { 
            !startSim && <div className="grid-title">Valores Alteráveis Durante a Simulação</div>
          }
          { onGameInputs.map((s, i) => <Slider key={`slider-${i}`} {...s} />) }
        </div>
        {
          !startSim ? 
          <Button variant="contained" color="primary" onClick={setStartSimulation}>
          Iniciar Simulação
          <PlayArrow />
          </Button>
          :
          (window.innerWidth > 850 || !showContent) &&
          <Button className="btn-restart" variant="contained" onClick={setRestartSimulation}>
          Recomeçar
          <Replay />
          </Button>
        }
      </div>
    </Style>
  )

}