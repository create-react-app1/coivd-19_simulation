import React, {useEffect, useState, useContext} from 'react'

import { EnvironmentContext }  from '../../services/contexts/environment'
import { Line } from 'react-chartjs-2'

import { IconButton } from '@material-ui/core';

import {
  Timeline
} from '@material-ui/icons/';

import Style from './style'

export default () => {

  const enviromentContext = useContext(EnvironmentContext)
  const { steps } = enviromentContext
  const {
    total, qttDeaths, qttAlives, qttInfecteds, qttMaskeds, qttStoppeds,
  } = enviromentContext.people

  const [chartData, setChartData] = useState({})
  const [lastStepDieds, setLastStepDied] = useState(0)
  const [lastStepInfected, setLastStepInfected] = useState(0)
  const [showContent, setShowContent] = useState(false)
  const [btnPulse, setBtnPulse] = useState(true)

  // TODO: fazer gráfico separado por dia "Mortes por dia", "infecções por dia"

  const chartConfig = [
    { label: "Vivas", cValue: qttAlives, lineColor: 'gray' },
    { label: "Mortes", cValue: qttDeaths, lineColor: 'pink' },
    // { label: "Media Mortes", cValue: qttDeaths/steps * total, lineColor: 'red' },
    { label: "Mortes por dia", cValue: qttDeaths - lastStepDieds, lineColor: 'crimson' },
    { label: "Infectadas", cValue: qttInfecteds, lineColor: 'green' },
    // { label: "Infectadas por dia", cValue: qttInfecteds - lastStepInfected, lineColor: 'aquamarine' },
    { label: "Com máscara", cValue: qttMaskeds, lineColor: 'blue' },
    { label: "Confinadas", cValue: qttStoppeds, lineColor: 'yellow' },
  ]

  const chart = () => {

    setLastStepDied(qttDeaths)
    setLastStepInfected(qttInfecteds)

    const newDatasets = chartConfig.map((data, i) => {
      const {label, lineColor, cValue} = data
      const lastDatasets = (chartData.datasets && chartData.datasets[i] && chartData.datasets[i].data) || []

      return {
        label,
        data: [...lastDatasets, cValue],
        borderColor: lineColor,
        backgroundColor: ['rgba(0,0,0,0)'],
        borderWidth: 4,
      }
    })

    setChartData({
      labels: setStepsLabel(),
      datasets: newDatasets
    })
  }

  const setStepsLabel = () => {
    let stepsList = []
    for(let i = 0; i < steps - 1; i++) stepsList.push(i + 1)
    return stepsList
  }

  useEffect(() => {
    if(enviromentContext.people.total) chart()
  }, [steps, enviromentContext.people.total])

  return (
    <Style className={`chart grid-wrapper ${showContent && 'show'}`}>
      <div className={
        `icon-button control-toggle-button 
        ${showContent && 'show'} 
        pulse-button ${btnPulse && 'pulse'}`}>
        <IconButton
          variant="contained"
          onClick={() => {setShowContent(!showContent); setBtnPulse(false);}} 
          color="primary" aria-label="upload picture" component="span"
        >
          <Timeline />
        </IconButton>
      </div>

      <div className="grid-block">
        <div className="grid-title">Estatisticas</div>
        <div className="grid-content">
          <div className="grid-item">Acompanhe as estatisticas no gráfico!</div>
          <Line data={chartData}
            options={{
              responsive: true,
              maintainAspectRatio: false,
              title: {text: "Geral", display: true},
              scales: {
                yAxes: [
                  {
                    ticks: {
                      autoSkip: true,
                    },
                    gridLines: {
                      display: false,
                    },                
                  }
                ]
              },
              elements: {
                point: {
                  radius: 1
                }
              }
            }}
          />
          <div className="grid-item">Esconda e mostre as linhas clicando nos retangulos, acima do gráfico.</div>
        </div>
      </div>
    </Style>
  )

}