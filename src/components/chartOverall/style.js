import styled from 'styled-components'

export default styled.div`

  canvas {
    max-width: 100%;
    width: auto !important;
    max-height: 300px;
    margin: 20px auto; 
  }

  @media (max-width: 850px) {
    .icon-button.control-toggle-button:not(.show) {
      position: fixed;
      bottom: 0px;
      right: 0px;

      transition: .5s;
    }

    .icon-button.control-toggle-button.show {
      position: absolute;
      margin: 5px;
      right: 5px;
    }

    &&.show {
      margin-left: -8px;
    }

  }

`