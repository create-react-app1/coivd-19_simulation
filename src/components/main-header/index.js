import React from 'react'

import virusIcon from '../../assets/virus.svg'

import Style from './style'

export default () => {

  return(
    <Style className="main-header">
      <div className="container">
        <img src={virusIcon} />
        COVID-19 Simulador
        <div className="developed-by">
        Desenvolvido por &nbsp;
        <a href="https://gdev.netlify.app/home" target="_blank">Giovanni P. Rosim</a>
        , 2020
        </div>
      </div>
    </Style>
  )

}