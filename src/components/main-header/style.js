import styled from 'styled-components'

export default styled.div`

  padding: 10px;
  background-color: #556955;
  margin: -8px;
  color: white;
  font-weight: bold;
  box-shadow: 0px 2px 1px 1px #000000a6;

  margin-bottom: 25px !important;

  .container {
    align-items: baseline;
  }

  img {
    height: 32px;
    width: 32px;
    margin-right: 3px;
  }

  .developed-by {
    font-size: 10px;
    margin-top: 5px;

    a {
      color: white;
      text-shadow: white 0px 0px 1px;
      font-weight: bold;
      font-size: 11px;
      text-decoration: none;
    }
  }

`