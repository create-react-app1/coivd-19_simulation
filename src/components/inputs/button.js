import React from 'react'

import { Button } from '@material-ui/core'


export default (props) => {
  return (
      <Button {...props}>{props.children}</Button>
  )
}
