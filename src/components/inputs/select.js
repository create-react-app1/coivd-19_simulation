import React from 'react'

import { Select } from '@material-ui/core'

export default (props) => {
  return (
      <Select {...props}>{props.children}</Select>
  )
}
