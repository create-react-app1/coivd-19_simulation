import React from 'react'

import { Slider } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import {CropDin} from '@material-ui/icons/';

import Style from './_style'

const useStyles = makeStyles({
  root: {
    width: 250,
  },
  input: {
    width: 42,
  },
});

export default ({
  sliderOptions = {min: 0, max: 100, step: 1, marks: [/*{value: '50', label: 'teste'}*/] },
  label = 'Título', 
  description = 'Descrição', 
  initValue = 0, 
  Icon = CropDin, 
  onChange
}) => {

  const classes = useStyles();
  const [value, setValue] = React.useState(initValue);

  const limitMarks = [
    {value: sliderOptions.min, label: sliderOptions.min},
    {value: sliderOptions.max, label: sliderOptions.max},
  ]

  const handleSliderChange = (event, newValue) => {
    setValue(getValueInsideLimits(newValue));
    onChange && onChange(getValueInsideLimits(newValue))
  };

  const handleInputChange = (event) => {
    setValue(event.target.value === '' ? '' : getValueInsideLimits(event.target.value));
    onChange && onChange(getValueInsideLimits(event.target.value))
  };

  const handleBlur = () => {
    if (value < sliderOptions.min) {
      setValue(sliderOptions.min);
    } else if (value > sliderOptions.max) {
      setValue(sliderOptions.max);
    }
  };

  const getValueInsideLimits = (newValue) => {
    newValue = Number(newValue)
    if (newValue < sliderOptions.min) return sliderOptions.min
    else if (newValue > sliderOptions.max) return sliderOptions.max

    return newValue
  }

  return (
    <Style className={classes.root + " input-wrapper"}>
      <Typography id="input-slider" className={'input-title'} gutterBottom>
        {label}
      </Typography>
      <Grid container spacing={2} alignItems="center">
        <Grid item>
          <Icon />
        </Grid>
        <Grid item xs>
          <Slider
            value={typeof value === 'number' ? value : sliderOptions.min}
            onChange={handleSliderChange}
            aria-labelledby="input-slider"
            step={sliderOptions.step}
            marks={limitMarks || sliderOptions.marks}
            min={sliderOptions.min}
            max={sliderOptions.max}
          />
        </Grid>
        <Grid item>
          <Input
            className={classes.input}
            value={value}
            margin="dense"
            onChange={handleInputChange}
            onBlur={handleBlur}
            inputProps={{
              type: 'number',
              'aria-labelledby': 'input-slider',
            }}
          />
        </Grid>
      </Grid>
      <Typography className={'input-description'} gutterBottom>
        {description}
      </Typography>
    </Style>
  )
}