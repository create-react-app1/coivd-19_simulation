import styled from 'styled-components'

export default styled.div`

  .input-title {
    font-size: 12px;
    font-weight: bold;
  }

  .input-description {
    font-size: 10px;
  }

  .MuiSlider-markLabel {
    color: rgba(0, 0, 0, 0.54) !important;
    font-size: 10px !important;
    line-height: 1 !important;
  }

`