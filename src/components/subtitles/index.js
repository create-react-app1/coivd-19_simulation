import React from 'react'

import Person from '../person'

import Style from './style'

export default () => {

  return (
    <Style className="subtitle grid-wrapper">
      <div className="grid-block">
        <div className="grid-title">Legenda</div>
        <div className="grid-content">
          <div className="grid-item"><Person /><span />Saudável</div>
          <div className="grid-item"><Person masked="true" /><span />Saudável Com Máscara</div>
          <div className="grid-item"><Person infected="true" /><span />Infectada</div>
          <div className="grid-item"><Person infected="true" masked="true" /><span />Infectada Com Máscara</div>
        </div>
      </div>
    </Style>
  )

}