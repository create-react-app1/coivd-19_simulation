import styled from 'styled-components'

export default styled.div`

  .grid-item {
    display: flex;
    font-size: 12px;
    align-items: center;

    span {
      margin: 0 5px;
    }
  }

  .grid-item + .grid-item {
    margin-top: 7px;
  }

`