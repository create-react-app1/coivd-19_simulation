import React, {useEffect, useContext} from 'react'

import { EnvironmentContext }  from '../../services/contexts/environment'

// Custom Hooks
import { useInterval } from '../../hooks/useInterval';

import Style from './style'

export default () => {

  const enviromentContext = useContext(EnvironmentContext)
  const { steps, setSteps } = enviromentContext
  const {
    total, qttDeaths, qttAlives, qttInfecteds, qttMaskeds, qttStoppeds,
    setQttDeaths, setQttAlives, setQttInfecteds, setQttMaskeds, setQttStoppeds,
  } = enviromentContext.people

  const setAllPeopleParams = () => {
    setQttStoppeds(window.people.filter(p => p.props.stopped && !p.props.dead).length)
    setQttMaskeds(window.people.filter(p => p.props.masked && !p.props.dead).length)
    setQttInfecteds(window.people.filter(p => p.props.infected && !p.props.dead).length)

    const deads = window.people.filter(p => p.props.dead).length
    setQttAlives(total - deads)
    setQttDeaths(deads)

    setSteps(steps + 1)
  }

  const infos = [
    {label: 'Pessoas Inicio', value: total,},
    {label: 'Vivas', value: qttAlives,},
    {label: 'Mortas', value: qttDeaths,},
    {label: 'Infectadas', value: qttInfecteds,},
    {label: 'Com máscara', value: qttMaskeds,},
    {label: 'Confinadas', value: qttStoppeds,},
  ]

  useInterval(setAllPeopleParams, 4000)

  return (
    <Style className="summary grid-wrapper">
      <div className="grid-block">
        <div className="grid-title">Resumo</div>
        <div className="grid-content">
        {
          infos.map((info, id) => 
          <div className="info grid-wrapper grid-block" key={id}>
            {info.label} <strong>{info.value}</strong>
          </div>)
        }
        </div>
      </div>
    </Style>
  )

}