import styled from 'styled-components'

export default styled.div`

  .grid-content {
    flex-wrap: wrap;
    justify-content: center;

    display: flex;
    align-self: center;

    background: none !important;

    width: 100%;
  }

  .info.grid-wrapper {
    margin: 2px;
    justify-content: center;
    align-items: center;

    width: 80px;
    height: 100%;
    display: grid;
    text-align: center;
  }

  .grid-block {
    height: 100%;
    display: flex;

    padding: 5px !important;
  }

  strong {
    margin: 0 5px;
    font-size: 14px;
  }

  @media (max-width: 1085px) {
    .info.grid-wrapper {
      width: 30%;
    }
  }

  @media (max-width: 850px) {
    .grid-block {
      display: block;
    }

    .grid-content {
      width: auto !important;  
      padding: 0px !important;
    }

    .info.grid-wrapper {
      width: 25%;
    }
  }

`