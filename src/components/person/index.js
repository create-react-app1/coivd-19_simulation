import React from 'react'

import Style from './style'

export default (props) => {

  const styleClasses = [
    'infected', 'masked'
  ]
  .map(c => props[c] ? c : null)
  .join(' ')
  .trim()

  return (
    <Style className={'person ' + styleClasses} />
  )
}