import styled from 'styled-components'

import maskIcon from '../../assets/coronavirus.svg'

const setSize = (size) => {
  return ['height', 'width'].map(p => `${p}: ${size || 16}px;`).join('')
}

export default styled.div`

  &&.person {
    ${props => setSize(props.size)}
    border-radius: 100%;
    border: 1px solid #0000008f;
  }

  .mask {
    display: none;
  }

  &&.masked::after {
    content: "";
    background-image: url(${maskIcon});
    ${props => setSize(props.size)}
    margin-top: 1px;
    position: absolute;
    background-position: center;
    background-size: cover;
  }

  &&.infected {
    background-color: #b7ff41;
  }

`