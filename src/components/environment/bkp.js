import React, { useState, useEffect, useContext } from 'react'

// Components
import { EnvironmentContext }  from '../../services/contexts/environment'
import Person from '../person'

import Style from './style'

// Custom Hooks
import { useInterval } from '../../hooks/useInterval';

export default ({containerSize, peopleInit}) => {

  const enviromentContext = useContext(EnvironmentContext)
  const containerContext = enviromentContext.container
  const peopleContext = enviromentContext.people

  const [people, setPeople] = useState([])

  const init = () => {
    // criar todas as pessoas e sortear suas posições
    const { total, stoppedPercent, maskedPercent, infectedPercent, } = peopleInit
    const { setTotal, setQttAlives, setQttStoppeds , setQttInfecteds, setQttMaskeds } = peopleContext

    // const maskedInit = Math.ceil(total * maskedPercent)
    // const infectedInit = Math.ceil(total * infectedPercent)
    // const stoppedInit = Math.ceil(total * stoppedPercent)

    setTotal(total)
    setQttAlives(total)
    // setQttMaskeds(maskedInit)
    // setQttInfecteds(infectedInit)
    // setQttStoppeds(stoppedInit)
  }

  const createPeople = () => {
    const { stoppedPercent, maskedPercent, infectedPercent, } = peopleInit
    const { total, setQttStoppeds, setQttInfecteds, setQttMaskeds } = peopleContext

    let auxPeople = []
    let totalInfected = 0
    let totalStopped = 0
    let totalMasked = 0
    for(let i = 0; i < total; i++) {
      const props = {
        infected: Math.random() <= infectedPercent,
        stopped: Math.random() <= stoppedPercent,
        masked: Math.random() <= maskedPercent,
      }

      if(props.infected) totalInfected++
      if(props.stopped) totalStopped++
      if(props.masked) totalMasked++

      auxPeople.push(
        <Person key={i} {...shufflePositionAndDirection()} {...props} />
      )
    }

    setQttMaskeds(totalMasked)
    setQttInfecteds(totalInfected)
    setQttStoppeds(totalStopped)

    setPeople(auxPeople)
  }

  const shufflePositionAndDirection = () => {
    const padding = 16

    const directionX = Math.random()
    const directionY = 1 - directionX

    return {
      x: Math.round((containerSize.width - padding * 2) * Math.random() + padding),
      y: Math.round((containerSize.height - padding * 2) * Math.random() + padding),
      direction: { x: directionX, y: directionY }
    }
  }


  const update = () => {

    const speed = 1

    const moviment = () => {

      setPeople(
        people.map( (p, i) => {
          // console.log(p)
          const newPosition = {
            x: p.props.x + (p.props.direction.x * speed),
            y: p.props.y + (p.props.direction.y * speed),
          }       

          return <Person key={i} {...p.props} {...newPosition} />
      }))

    }

    const testContact = () => {

    }

    moviment()

    // setPeople()
    // verificar contato
    // verificar atualização geral (mortes, tempo invencivel, resultado da infecção...)
  }

  useEffect(() => {
    containerContext.changeSize(containerSize)
    init()
    
  }, [])

  useEffect(() => {
    createPeople()
  }, [peopleContext.total])

  useInterval(() => {
    update()
  }, 60/1000)

  return (
      <Style size={containerSize}>
        <h1>Environment!</h1>

        {/* {people} */}
        {containerContext.size.width + ' | ' + containerContext.size.height}
        <div>
        {peopleContext.total + ' | ' + peopleContext.qttInfecteds + ' | ' + peopleContext.qttMaskeds + ' | ' + peopleContext.qttStoppeds}
        </div>
      </Style>
  )
}