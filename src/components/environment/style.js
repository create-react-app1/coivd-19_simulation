import styled from 'styled-components'

const setSize = (size) => {
  return [
    'height', 'width'
  ].map(p => size[p] ? `${p}: ${size[p]}px;` : null)
  .join('')
}

export default styled.div`
  position: relative;

  ${props => setSize(props.size)}

  overflow: hidden;
  background-color: white !important;

  display: flex;

  #main-canvas {
    display: flex;
    margin: auto;
  }

`