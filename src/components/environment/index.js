import React, { useEffect, useContext, memo, } from 'react'

// Components
import { EnvironmentContext }  from '../../services/contexts/environment'

import Style from './style'

// Custom Hooks
import { useInterval } from '../../hooks/useInterval';

import maskIcon from '../../assets/coronavirus.svg'

window.canvas = {}
window.ctx = {}
window.people = []

window.frameRate = 1000/30

const Environment = ({containerSize, peopleInit, params}) => {

  const enviromentContext = useContext(EnvironmentContext)
  // const containerContext = enviromentContext.container
  const peopleContext = enviromentContext.people

  useEffect(() => {
    initGame()
  }, [])

  let mask = new Image()
  mask.src = maskIcon

  const initGame = () => {
    window.canvas = document.getElementById("main-canvas")
    window.ctx = window.canvas.getContext('2d');
    window.ctx.globalCompositeOperation = 'destination-over'; // way to render
    // Reference: https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/globalCompositeOperation

    const { total, stoppedPercent, maskedPercent, infectedPercent, } = peopleInit
    const { setTotal, setQttAlives, setQttStoppeds, setQttInfecteds, setQttMaskeds, setQttDeaths } = peopleContext

    let totalInfected = 0
    let totalStopped = 0
    let totalMasked = 0

    for (let i = 0; i < total; i++) {
      const props = {
        infected: Math.random() <= infectedPercent,
        stopped: Math.random() <= stoppedPercent,
        masked: Math.random() <= maskedPercent,
        dead: false
      }

      const status = {
        recoveryTime: props.infected ? suffleRecoveryTime() : 0, // tempo para recuperar da doença
        invencibleTime: 0, // invencible after contact
      }

      if(props.infected) totalInfected++
      if(props.stopped) totalStopped++
      if(props.masked) totalMasked++

      window.people.push({
        ...shufflePositionAndDirection(),
        props,
        status,
      })
    }

    setTotal(total)
    setQttAlives(total)
    setQttDeaths(0)
    setQttMaskeds(totalMasked)
    setQttInfecteds(totalInfected)
    setQttStoppeds(totalStopped)
    
  }

  const shufflePositionAndDirection = () => {
    const padding = 16

    const directionX = Math.random() * (Math.random() >= .5 ? 1 : -1)
    const directionY = (1 - directionX) * (Math.random() >= .5 ? 1 : -1)

    return {
      position: {
        x: Math.round((containerSize.width - padding * 2) * Math.random() + padding),
        y: Math.round((containerSize.height - padding * 2) * Math.random() + padding),
      },
      direction: { x: directionX, y: directionY }
    }
  }

  const loopGame = () => {
  
    if(!window.ctx.clearRect) return

    window.ctx.clearRect(0,0,containerSize.width,containerSize.height);  // clear canvas
    window.people.forEach(
      (p, i) => {
        update(p, i)
        draw(p, i)
      }
    )

  }

  const draw = (p) => {
    createPerson({...p})
  }

  const createPerson = ({ position, props }) => {
    let ctx = window.ctx
    
    if(props.dead) return 

    props.masked && ctx.drawImage(mask,position.x-8,position.y-8,16,16);

    ctx.beginPath();
    ctx.fillStyle = props.infected ? "#b7ff41" : "#fff";
    ctx.arc(position.x,position.y, 8, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.fill();
    ctx.closePath();
  }

  const update = (p, i) => {
    if(p.props.dead) return
    moviment(p)

    if(!p.status.invencibleTime) contact(p, i)
    else {
      const auxNextTime = p.status.invencibleTime - window.frameRate
      const nextTime = auxNextTime > 0 ? auxNextTime : 0
      p.status.invencibleTime = nextTime

    }

    if(p.status.recoveryTime) {
      const deathTax = params.deathTax || .15

      const auxNextTime = p.status.recoveryTime - window.frameRate
      const nextTime = auxNextTime > 0 ? auxNextTime : 0
      p.status.recoveryTime = nextTime

      if(p.status.recoveryTime == 0) {
        p.props.dead = Math.random() <= deathTax
        if(p.props.dead) {

          // console.error("A pessoa ", i, " morreu por causa da COIVD.", p.props)

        }
        else {
          p.props.infected = false
          tryUseConscience(p)
          // console.warn("A pessoa ", i, " sobreviveu ao COIVD.")
        }

      }
    }
  }

  const moviment = (p) => {

    if(p.props.stopped) return

    const speed = 3

    let auxPosition = {
      x: p.position.x + (p.direction.x * speed),
      y: p.position.y + (p.direction.y * speed),
    }
    
    if(auxPosition.x < 0 || auxPosition.x > containerSize.width) p.direction.x *= -1
    if(auxPosition.y < 0 || auxPosition.y > containerSize.height) p.direction.y *= -1

    const newPosition = {
      x: p.position.x + (p.direction.x * speed * window.deltaTime),
      y: p.position.y + (p.direction.y * speed * window.deltaTime),
    }

    p.position = newPosition

  }

  const contact = (currentPerson, currentIndex) => {
    const contactRadius = params.contactRadius || 32

    window.people.forEach((anotherPerson, anotherIndex) => {

      if(
        (currentIndex === anotherIndex) || 
        (currentPerson.props.infected === anotherPerson.props.infected) ||
        anotherPerson.props.dead
      ) return


      const hypot  = (
        Math.hypot(
          currentPerson.position.x - anotherPerson.position.x, 
          currentPerson.position.y - anotherPerson.position.y
        )
      )

      if(hypot <= contactRadius) {
        let infectedPerson = currentPerson
        let normalPerson = anotherPerson

        if(anotherPerson.props.infected) {
          infectedPerson = anotherPerson
          normalPerson = currentPerson
        }

        const infectionTax = params.infectionTax || .5
        const maskProtectionTax = params.maskProtectionTax || .9

        const protectionBetweenPeople = 
        (infectedPerson.props.masked ? (1 - maskProtectionTax) : 1)
        *
        (normalPerson.props.masked ? (1 - maskProtectionTax) : 1)

        const infectionRatio = infectionTax * protectionBetweenPeople
        const randomValue = Math.random()
        if(randomValue <= infectionRatio) {
          // console.log(`a pessoa ${" " || normalPerson} foi contagiada! `, normalPerson.props.masked, infectedPerson.props.masked)
          normalPerson.props.infected = true
          
          normalPerson.status.recoveryTime = suffleRecoveryTime()

            tryUseConscience(normalPerson)
        }
        else {
          normalPerson.status.invencibleTime = params.invencibleTime || 10
          infectedPerson.status.invencibleTime = params.invencibleTime || 10
        }
      }

    })
  }

  const suffleRecoveryTime = () => {
    const recoveryTimeAverage = params.virusTime || 100

    const virusTimeRange = params.virusTimeRange || 0
    const rangeTimeVirus = recoveryTimeAverage * (virusTimeRange * 2)

    return ((1-virusTimeRange) * recoveryTimeAverage) + Math.random() * rangeTimeVirus
  }

  const tryUseConscience = (p) => {
    const conscienceTax = params.conscienceTax || 0

    const willBeStop = !p.props.stopped && Math.random() <= (conscienceTax * (params.stoppedTaxRate || 0))
    const willBeMask = !p.props.masked && Math.random() <= (conscienceTax * (params.useMaskTaxRate || 0))

    p.props.stopped = p.props.stopped || willBeStop
    p.props.masked = p.props.masked || willBeMask

  }

  window.lastTime = (new Date).getTime()
  useInterval(() => { 
    var newDate = (new Date).getTime()
    var dateDiff = (newDate - window.lastTime)
    window.lastTime = newDate
    window.deltaTime = dateDiff / (window.frameRate)

    loopGame() 
  }, window.frameRate)

  return (
    <Style className="simulator grid-wrapper" size={containerSize}>
      <canvas id="main-canvas" width={containerSize.width} height={containerSize.height} />
    </Style>
  )
} 

export default memo(Environment)