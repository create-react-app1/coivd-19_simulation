import React, { useState, useEffect, useContext } from 'react'

// Components
import { EnvironmentContext }  from '../../services/contexts/environment'
import Person from '../person'

import Style from './style'

// Custom Hooks
import { useInterval } from '../../hooks/useInterval';

import maskIcon from '../../assets/coronavirus.svg'

export default ({containerSize, peopleInit, params}) => {

  const enviromentContext = useContext(EnvironmentContext)
  const containerContext = enviromentContext.container
  const peopleContext = enviromentContext.people

  const [canvas, setCanvas] = useState()
  const [ctx, setCtx] = useState()

  useEffect(() => {
    initGame()
    // setInterval(loopGame, 60/1000)
  }, [])

  // let intervalId 
  // useEffect(() => {
  //   if(intervalId) return
  //   intervalId = useInterval(loopGame, 60/1000)
  // }, [ctx])


  let people = []

  let mask = new Image()
  mask.src = maskIcon
  const initGame = () => {
    // canvas = document.getElementById("main-canvas")
    // ctx = canvas.getContext('2d');
    // ctx.globalCompositeOperation = 'destination-over'; // way to render
    // Reference: https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/globalCompositeOperation
    
    const auxCanvas = document.getElementById("main-canvas")

    setCanvas(auxCanvas)
    setCtx(auxCanvas.getContext('2d'))

    const { total, stoppedPercent, maskedPercent, infectedPercent, } = peopleInit
    const { setTotal, setQttAlives, setQttStoppeds, setQttInfecteds, setQttMaskeds, setQttDeaths } = peopleContext

    let totalInfected = 0
    let totalStopped = 0
    let totalMasked = 0

    for (let i = 0; i < total; i++) {
    // TEST CODE
    // for (let i = 0; i < 2; i++) {
      const props = {
        infected: Math.random() <= infectedPercent,
        stopped: Math.random() <= stoppedPercent,
        masked: Math.random() <= maskedPercent,
        dead: false
      }
      // TEST CODE
      // const props = {
      //   infected: i==0,
      //   stopped: true,
      //   masked: false,
      //   dead: false
      // }

      const status = {
        recoveryTime: props.infected ? 100 : 0, // tempo para recuperar da doença
        invencibleTime: 0, // invencible after contact
      }

      if(props.infected) totalInfected++
      if(props.stopped) totalStopped++
      if(props.masked) totalMasked++

      people.push({
        ...shufflePositionAndDirection(),
        props,
        status,
      })
    }

    setTotal(total)
    setQttAlives(total)
    setQttDeaths(0)
    setQttMaskeds(totalMasked)
    setQttInfecteds(totalInfected)
    setQttStoppeds(totalStopped)

    // setInterval(loopGame, 60/1000)
    // setInterval(() => {
    //   testeLoop()
    // }, 1000)

    // useInterval(loopGame, 60/1000)

    
  }

  

  // useInterval(testeLoop, 1000)
  // function testeLoop() {
  //   console.log(peopleContext, peopleContext.total)
  // }

  const shufflePositionAndDirection = () => {
    const padding = 16

    const directionX = Math.random() * (Math.random() >= .5 ? 1 : -1)
    const directionY = (1 - directionX) * (Math.random() >= .5 ? 1 : -1)

    return {
      position: {
        x: Math.round((containerSize.width - padding * 2) * Math.random() + padding),
        y: Math.round((containerSize.height - padding * 2) * Math.random() + padding),
      },
      // TEST CODE
      // position: {
      //   x: 150 + Math.random() * 15,
      //   y: 150 + Math.random() * 15,
      // },
      direction: { x: directionX, y: directionY }
    }
  }

  const loopGame = () => {

    ctx.clearRect(0,0,containerSize.width,containerSize.height);  // clear canvas
    people.forEach(
      (p, i) => {
        update(p, i)
        draw(p, i)
      }
    )

  }

  const draw = (p) => {
    createPerson({...p})
  }

  const createPerson = ({ position, props }) => {
    
    if(props.dead) return 

    props.masked && ctx.drawImage(mask,position.x-8,position.y-8,16,16);

    ctx.beginPath();
    ctx.fillStyle = props.infected ? "#b7ff41" : "#fff";
    ctx.arc(position.x,position.y, 8, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.fill();
    ctx.closePath();
  }

  const update = (p, i) => {
    if(p.props.dead) return
    moviment(p)

    if(!p.status.invencibleTime) contact(p, i)
    else {
      const auxNextTime = p.status.invencibleTime - 60/1000
      const nextTime = auxNextTime > 0 ? auxNextTime : 0
      p.status.invencibleTime = nextTime
      // if(i==0) {
      //   console.log("meu tempo invencivel...", p.status.invencibleTime)
      //   if(nextTime == 0) console.warn("estou normal")
      // }
    }

    if(p.status.recoveryTime) {
      const deathTax = params.deathTax || .15

      const auxNextTime = p.status.recoveryTime - 60/1000
      const nextTime = auxNextTime > 0 ? auxNextTime : 0
      p.status.recoveryTime = nextTime

      if(p.status.recoveryTime == 0) {
        p.props.dead = Math.random() <= deathTax
        if(p.props.dead) {

          // const enviromentContext = useContext(EnvironmentContext)
          // const peopleContext = enviromentContext.people

          // console.error("A pessoa ", i, " morreu por causa da COIVD.", peopleContext)
          peopleContext.setQttDeaths(peopleContext.qttDeaths++)
          peopleContext.setQttAlives(peopleContext.qttAlives--)
        }
        else {
          p.props.infected = false
          peopleContext.setQttInfecteds(peopleContext.qttInfecteds--)
          // console.warn("A pessoa ", i, " sobreviveu ao COIVD.")
        }
      }
    }
  }

  const moviment = (p) => {

    if(p.props.stopped) return

    const speed = 1

    let auxPosition = {
      x: p.position.x + (p.direction.x * speed),
      y: p.position.y + (p.direction.y * speed),
    }
    
    if(auxPosition.x < 0 || auxPosition.x > containerSize.width) p.direction.x *= -1
    if(auxPosition.y < 0 || auxPosition.y > containerSize.height) p.direction.y *= -1

    const newPosition = {
      x: p.position.x + (p.direction.x * speed),
      y: p.position.y + (p.direction.y * speed),
    }

    p.position = newPosition

  }

  const contact = (currentPerson, currentIndex) => {
    const contactRadius = params.contactRadius || 32

    people.forEach((anotherPerson, anotherIndex) => {

      if(
        (currentIndex === anotherIndex) || 
        (currentPerson.props.infected === anotherPerson.props.infected) ||
        anotherPerson.props.dead
      ) return


      const hypot  = (
        Math.hypot(
          currentPerson.position.x - anotherPerson.position.x, 
          currentPerson.position.y - anotherPerson.position.y
        )
      )

      if(hypot <= contactRadius) {
        let infectedPerson = currentPerson
        let normalPerson = anotherPerson

        if(anotherPerson.props.infected) {
          infectedPerson = anotherPerson
          normalPerson = currentPerson
        }

        const infectionTax = params.infectionTax || .5
        const maskProtectionTax = params.maskProtectionTax || .9

        const recoveryTimeAverage = params.virusTime || 100

        const protectionBetweenPeople = 
        (infectedPerson.props.masked ? (1 - maskProtectionTax) : 1)
        *
        (normalPerson.props.masked ? (1 - maskProtectionTax) : 1)

        const infectionRatio = infectionTax * protectionBetweenPeople
        const randomValue = Math.random()
        if(randomValue <= infectionRatio) {
          // console.log(`a pessoa ${" " || normalPerson} foi contagiada! `, normalPerson.props.masked, infectedPerson.props.masked)
          normalPerson.props.infected = true
          
          peopleContext.setQttInfecteds(peopleContext.qttInfecteds++)

          const virusTimeRange = params.virusTimeRange || 0
          const rangeTimeVirus = recoveryTimeAverage * (virusTimeRange * 2)

          normalPerson.status.recoveryTime = 
            ((1-virusTimeRange) * recoveryTimeAverage) + Math.random() * rangeTimeVirus
        }
        else {
          normalPerson.status.invencibleTime = params.invencibleTime || 10
          infectedPerson.status.invencibleTime = params.invencibleTime || 10
        }
      }

    })
  }

  useInterval(loopGame, 1000)

  return (
      <Style size={containerSize}>

        <canvas id="main-canvas" width={containerSize.width} height={containerSize.height} />

      </Style>
  )
} 