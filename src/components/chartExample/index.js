import React, {useEffect, useState, useContext} from 'react'

import { EnvironmentContext }  from '../../services/contexts/environment'
import { Line } from 'react-chartjs-2'

// References:
// https://www.chartjs.org/
// https://www.youtube.com/watch?v=A5KaLpqzRi4

export default () => {

  const enviromentContext = useContext(EnvironmentContext)
  // const {
  //   total, qttDeaths, qttAlives, qttInfecteds, qttMaskeds, qttStoppeds,
  //   setQttDeaths, setQttAlives, setQttInfecteds, setQttMaskeds, setQttStoppeds,
  // } = enviromentContext.people

  const [chartData, setChartData] = useState({})

  const chart = () => {
    setChartData({
      labels: ['segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sabado', 'domingo'],
      datasets: [
        {
          label: 'teste',
          data: [20, 40, 30, 33, 22, 44, 66, 50],
          backgroundColor: ['rgba(75,192,192,0)'],
          background: 'none',
          borderWidth: 4,
          borderColor: 'red'
        },
        {
          label: 'teste 2',
          data: [2, 20, 10, 3, 42, 74, 56, 30],
          backgroundColor: ['rgba(75,192,192,0)'],
          background: 'none',
          borderWidth: 4,
          borderColor: 'green'
        }
      ]
    })
  }

  useEffect(() => {
    chart()
  }, [])

  return (
    <div>
      <Line data={chartData} 
        options={{
          responsive: true,
          title: {text: "Teste!", display: true},
          scales: {
            yAxes: [
              {
                ticks: {
                  autoSkip: true,
                  maxTicksLimit: 10,
                  beginAtZero: true,
                },
                gridLines: {
                  display: false,
                }
                
              }
            ]
          }
        }}
      />
    </div>
  )

}