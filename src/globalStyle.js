import { createGlobalStyle } from "styled-components"

import virus from "./assets/virus.svg"

export default createGlobalStyle`

  body {
    background-image: url(${virus});
    background-size: 80%;
    background-attachment: fixed;
    background-blend-mode: soft-light;
    background-color: gainsboro;
  }

  @media (max-width: 850px) {

    body {
      padding-bottom: 50px;
    }

  }

`